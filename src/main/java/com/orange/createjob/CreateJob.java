package com.orange.createjob;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateJob {

    WebDriver webDriver;

    @FindBy(xpath = "/html/body/div[1]/div[2]/ul/li[1]/a/b")
    private WebElement tabAdmin;

    @FindBy(id = "menu_admin_Job")
    private WebElement itemJob;

    @FindBy(id = "menu_admin_viewJobTitleList")
    private WebElement itemJobTitle;

    @FindBy(id = "btnAdd")
    private WebElement btnAddJob;

    @FindBy(id = "jobTitle_jobTitle")
    private WebElement txtJobTitle;

    @FindBy(id = "jobTitle_jobDescription")
    private WebElement txtJobDescription;

    @FindBy(id = "jobTitle_jobSpec")
    private WebElement itemImage;

    @FindBy(id = "jobTitle_note")
    private WebElement txtJobNote;

    @FindBy(id = "btnSave")
    private WebElement btnSave;

    public CreateJob(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void doCreateJobTitle(String JobTitle, String JobDescription, String ChooseFile, String JobNote) {

        tabAdmin.click();
        itemJob.click();
        itemJobTitle.click();
        btnAddJob.click();
        txtJobTitle.sendKeys(JobTitle);
        txtJobDescription.sendKeys(JobDescription);
        //    itemImage.sendKeys(ChooseFile);
        txtJobNote.sendKeys(JobNote);
        btnSave.click();

    }

    public void doUpdateJobTitle(String JobTitle, String JobDescription, String ChooseFile, String JobNote) {

        btnSave.click();
        txtJobTitle.clear();
        txtJobTitle.sendKeys(JobTitle);
        txtJobDescription.clear();
        txtJobDescription.sendKeys(JobDescription);
        //    itemImage.sendKeys(ChooseFile);
        txtJobNote.clear();
        txtJobNote.sendKeys(JobNote);
        btnSave.click();

    }


}
