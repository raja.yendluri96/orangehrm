package com.orange.createjob;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.File;

public class FakeData {

    WebDriver webDriver;
    private String jobTitleText;
    private String jobDescription;
    private String imagePath;
    private String jobNote;


    public FakeData(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
        setData();
    }

    private void setData() {
        Faker faker = new Faker();
        setjobTitleText(faker.job().title());
        setJobDescription(faker.job().keySkills());
        File file = new File("src/main/java/com/orange/createjob/Config_Data/large_sample.png");
        setImagePath(file.getPath());
        setJobNote(faker.job().seniority());
    }

    public String getjobTitleText() {
        return jobTitleText;
    }

    public void setjobTitleText(String jobTitleText) {
        this.jobTitleText = jobTitleText;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getJobNote() {
        return jobNote;
    }

    public void setJobNote(String jobNote) {
        this.jobNote = jobNote;
    }

}
