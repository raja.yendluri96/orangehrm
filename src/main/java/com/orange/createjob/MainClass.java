package com.orange.createjob;
import org.testng.annotations.Test;

public class MainClass extends DriverSetup {


   static FakeData data = new FakeData(driver);
   public final static String jobTitle = data.getjobTitleText();


    @Test(priority = 1)
    public void testLogin() {
        Loginpage loginpage = new Loginpage(driver);
        loginpage.doLogin("Admin", "admin123");
    }

    @Test(priority = 2)
    public void testCreateJob() {
        CreateJob createJob = new CreateJob(driver);
        createJob.doCreateJobTitle(jobTitle, data.getJobDescription(), data.getImagePath(), data.getJobNote());
    }

    @Test(priority = 3)
    public void testVerifyJobTile() {
        VerifyStatuses verifyStatuses = new VerifyStatuses(driver);
        verifyStatuses.validateJobCreated();
    }

    @Test(priority = 4)
    public void testUpdateJob() {
        UpdateDeleteJob updateDeleteJob = new UpdateDeleteJob(driver);
        updateDeleteJob.UpdateJobTitle();
    }

    @Test(priority = 5)
    public void testVerifyJobUpdate() {
        VerifyStatuses verifyStatuses = new VerifyStatuses(driver);
        verifyStatuses.validateJobUpdated();

    }

    @Test(priority = 6)
    public void testJobTitleDelete() {
        UpdateDeleteJob deleteJob = new UpdateDeleteJob(driver);
        deleteJob.DeleteJobTitle();
    }

    @Test(priority = 7)
    public void testVerifyJobDelete() {
        VerifyStatuses verifyStatuses = new VerifyStatuses(driver);
        verifyStatuses.validateJobDeleted();

    }


}
