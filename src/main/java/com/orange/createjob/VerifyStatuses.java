package com.orange.createjob;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VerifyStatuses {

    WebDriver webDriver;

    @FindBy(xpath = "/html/body/div[1]/div[3]/div[1]/div/div[2]/form/div[2]")
    private WebElement txtJobSuccessMessage;

    @FindBy(xpath = "/html/body/div[1]/div[3]/div[1]/div/div[2]/form/div[2]")
    private WebElement txtJobUpdatesMessage;

    @FindBy(xpath = "/html/body/div[1]/div[3]/div[1]/div/div[2]/form/div[2]")
    private WebElement txtJobDeleteMessage;

    public VerifyStatuses(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void validateJobCreated() {
        if (txtJobSuccessMessage.isDisplayed())
            System.out.println(txtJobSuccessMessage.getText());
    }

    public void validateJobUpdated() {
        if (txtJobUpdatesMessage.isDisplayed())
            System.out.println(txtJobUpdatesMessage.getText());
    }

    public void validateJobDeleted() {
        if (txtJobDeleteMessage.isDisplayed())
            System.out.println(txtJobDeleteMessage.getText());
    }
}
