package com.orange.createjob;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.orange.createjob.MainClass.jobTitle;


public class UpdateDeleteJob {


    static WebDriver webDriver;
    static FakeData data = new FakeData(webDriver);

    public final static String JobTitle2 = data.getjobTitleText();

    @FindBy(id = "btnDelete")
    private WebElement btnDelete;

    @FindBy(id = "dialogDeleteBtn")
    private WebElement dialogDeleteBtn;

    public UpdateDeleteJob(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void UpdateJobTitle() {

        CreateJob createJob = new CreateJob(webDriver);
        WebElement elementcreatedJob = webDriver.findElement(By.xpath("//a[contains(text(),'" + jobTitle + "')]"));
        elementcreatedJob.click();
        createJob.doUpdateJobTitle(JobTitle2, data.getJobDescription(), data.getImagePath(), data.getJobNote());
    }

    public void DeleteJobTitle() {
        WebElement ItemDeleteCheckBox = webDriver.findElement(By.xpath("//a[contains(text(), '" + JobTitle2 + "')]/parent::td/preceding-sibling::td/input"));
        ItemDeleteCheckBox.click();
        btnDelete.click();
        dialogDeleteBtn.click();
    }
}
