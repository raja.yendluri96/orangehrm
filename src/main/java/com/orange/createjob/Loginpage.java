package com.orange.createjob;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Loginpage {

    WebDriver webDriver;

    @FindBy(id = "txtUsername")
    private WebElement txt_userName;

    @FindBy(id = "txtPassword")
    private WebElement txt_passWord;

    @FindBy(id = "btnLogin")
    private WebElement btnLogin;

    public Loginpage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void doLogin(String Username, String Password) {
        txt_userName.sendKeys(Username);
        txt_passWord.sendKeys(Password);
        btnLogin.click();
    }


}
